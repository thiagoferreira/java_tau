package strings;

public class Password {

    public static void main(String[] args) {
        String passwordValidation = validatePassword("thiago",
                "123456", "T234567a");
        System.out.println(passwordValidation);
    }

    public static String validatePassword(String username, String oldPassword, String newPassword){
        String errorMessage = "";
        if (newPassword == username){
            errorMessage += "\n Password can't be the username";
        } else if (newPassword == oldPassword) {
            errorMessage += "\n Password can't be equal the current password";
        } else if (newPassword.length() < 8) {
            errorMessage += "\n Password must have at least 8 characters";
        } else if (newPassword.length() >= 8) {
            boolean existUpperCase = false;
            boolean existLowerCase = false;
            for (int i = 0; i < newPassword.length(); i++) {
                if (Character.isLowerCase(newPassword.charAt(i))) {
                    existLowerCase = true;
                } else if (Character.isUpperCase(newPassword.charAt(i))) {
                    existUpperCase = true;
                }
            }
            if (existLowerCase == false) {
                errorMessage += "\n Password must have at least a character in Lowercase";
            }
            if (existUpperCase == false){
                errorMessage += "\n Password must have at least a character in Uppercase";
            }
        }
        if (errorMessage == ""){
            return  "Valid Password";
        }
        return errorMessage;
    }
}
