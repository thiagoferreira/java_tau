package polymorphism;

public class Zoo {

    public static void main(String[] args) {
        Animal hunter = new Dog();
        hunter.makeSound();
        feed(hunter);

        hunter = new Cat();
        hunter.makeSound();
        feed(hunter);
    }

    public static void feed(Animal animal){
        if(animal instanceof Dog){
            System.out.println("here's your dog food");
        } else if (animal instanceof Cat) {
            System.out.println("here's your cat food");
        }
    }
}
