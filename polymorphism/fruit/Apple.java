package polymorphism.fruit;

public class Apple extends Fruit{
    public Apple(double calories){
        setCalories(calories);
    }

    public void removeSeeds(){
        System.out.println("Seeds were removed");
    }

    @Override
    public void makeJuice(){
        System.out.println("Apple juice is made");
    }
}
