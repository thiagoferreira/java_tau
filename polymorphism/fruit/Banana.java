package polymorphism.fruit;

public class Banana extends Fruit{
    public Banana(double calories){
        setCalories(calories);
    }

    public void peel(){
        System.out.println("Peel was removed");
    }

    @Override
    public void makeJuice(){
        System.out.println("Banana juice is made");
    }
}
