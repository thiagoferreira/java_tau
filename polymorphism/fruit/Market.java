package polymorphism.fruit;

public class Market {

    public static void main(String[] args) {
        Banana b1 = new Banana(10);
        b1.peel();
        b1.makeJuice();
        Apple a1 = new Apple(2);
        a1.removeSeeds();
        a1.makeJuice();

        Fruit b2 = new Banana(8);
        ((Banana) b2).peel();
        b2.makeJuice();

    }
}
