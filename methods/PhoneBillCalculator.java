package methods;

import java.util.Scanner;

public class PhoneBillCalculator {

    static double valueByMinute = 0.25;
    static double tax = 15;
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        double planValue = getPlanValue();
        int minutes = getAverageMinutes();
        scanner.close();

        double minutesCost = calculateMinutesCost(minutes);
        double subtotal = planValue + minutesCost;
        double taxValue = calculateTaxValue(subtotal);
        double total = subtotal + taxValue;

        System.out.println("Phone Bill Statement");
        System.out.println("Plan: $"+planValue);
        System.out.println("Overage: $"+minutesCost);
        System.out.println("Tax: $"+taxValue);
        System.out.println("Total: $"+total);
    }

    public static double getPlanValue(){
        System.out.println("Enter base cost of the plan:");
        double planValue = scanner.nextDouble();
        return planValue;
    }

    public static int getAverageMinutes(){
        System.out.println("Enter overage minutes:");
        int averageMinutes = scanner.nextInt();
        return averageMinutes;
    }

    public static double calculateMinutesCost(int minutes){
        double total = minutes * valueByMinute;
        return total;
    }

    public static double calculateTaxValue(double subtotal){
        double percent = tax/100;
        double total = subtotal * percent;
        return total;
    }
}
