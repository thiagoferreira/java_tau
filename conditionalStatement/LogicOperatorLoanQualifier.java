package conditionalStatement;

import java.util.Scanner;

public class LogicOperatorLoanQualifier {
    public static void main(String args []){
        int reqSalary = 30000;
        int reqYearsEmployed = 2;

        System.out.println("Enter your salary");
        Scanner scanner = new Scanner(System.in);
        double salary = scanner.nextDouble();

        System.out.println("Enter the number of years with your current employer");
        double jobYears = scanner.nextDouble();
        scanner.close();

        if(salary >= reqSalary && jobYears >= reqYearsEmployed){
            System.out.println("Congrats! You qualify for the loan");
        }
        else{
            System.out.println("Sorry you don`t meet the requirements, is necessary to have a salary of "+reqSalary
            +" \n And Is necessary to have  "+reqYearsEmployed+" years on the same job");
        }
    }
}
