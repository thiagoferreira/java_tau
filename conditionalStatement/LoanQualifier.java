package conditionalStatement;

import java.util.Scanner;

public class LoanQualifier {
    public static void main(String args []){
        int reqSalary = 30000;
        int reqYearsEmployed = 2;

        System.out.println("Enter your salary");
        Scanner scanner = new Scanner(System.in);
        double salary = scanner.nextDouble();

        System.out.println("Enter the number of years with your current employer");
        double jobYears = scanner.nextDouble();
        scanner.close();

        if(salary >= reqSalary){
            if(jobYears >= reqYearsEmployed){
                System.out.println("Congrats! You qualify for the loan");
            }
            else {
                System.out.println(""" 
                        You don`t have the number of years employed necessary on the current job,
                        Is necessary to have  """+" "+reqYearsEmployed+" years on the same job");
            }
        }
        else{
            System.out.println(""" 
                        Your salary isn`t enough to get the loan,
                        Is necessary to have a salary of """+" "+reqSalary);
        }
    }
}
