package conditionalStatement;

import java.util.Scanner;

public class DollarGame {

    public static void main(String args []){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to 'Change for a Dollar'! " +
                "Your goal is to enter enough change to make exactly $1.00");
        System.out.println("How many pennies would you like?");
        int pennies = scanner.nextInt();
        System.out.println("How many nickels would you like?");
        int nickels = scanner.nextInt();
        System.out.println("How many dimes?");
        int dimes = scanner.nextInt();
        System.out.println("How many quarters?");
        int quarters = scanner.nextInt();
        scanner.close();

        int oneDol = 1;
        double total = (pennies * 0.01) + (nickels * 0.05) + (dimes * 0.1) + (quarters * 0.25);

        if(total == oneDol){
            System.out.println("You won the game :D");
        }
        else if(total > oneDol){
            double overValue = total - oneDol;
            System.out.println("You lose :( ! You went over: "+ overValue +"$");
        }
        else{
            double underValue = oneDol - total;
            System.out.println("You lose :( ! You went under: "+ underValue +"$");
        }
    }
}
