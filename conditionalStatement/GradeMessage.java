package conditionalStatement;

import java.util.Scanner;

public class GradeMessage {
    public static void main(String args[]){
        System.out.println("Enter your letter grade");
        Scanner scanner = new Scanner(System.in);
        String grade = scanner.next();
        scanner.close();
        grade = grade.toUpperCase();

        String message;

        switch(grade){
            case "A" -> message = "Excellent Job!";
            case "B" -> message = "Great Job!";
            case "C" -> message = "Good Job!";
            case "D" -> message = "You need to work a bit harder";
            case "F" -> message = "Uh oh!";
            default -> message = "Error! Invalid Grade";
        }
        System.out.println(message);
    }
}
