package conditionalStatement;

import java.util.Scanner;

public class QuotaCalculator {
    public static void main(String args []){
        int quota = 10;

        System.out.println("Enter the number of sales you made this week");
        Scanner scanner = new Scanner(System.in);
        int numberSales = scanner.nextInt();
        scanner.close();

        if(numberSales>=quota){
            System.out.println("Congratulations you achieved the goal");
        }
        else {
            int salesShort = quota - numberSales;
            System.out.println("You need to improve the number of sales, you were "+salesShort+" sales short");
        }
    }
}
