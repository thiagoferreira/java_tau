package classAndObjects.phoneBill;

public class Calculator{

    double valueByMinute = 0.25;
    double tax = 15;

    public double calculateExMinutesCost(int minutesUsed, int allottedMinutes){
        if(minutesUsed <= allottedMinutes){
            return 0;
        }
        double overageMinutes = minutesUsed - allottedMinutes;
        return overageMinutes * valueByMinute;
    }

    public double calculateTaxValue(double subtotal){
        double percent = tax/100;
        double total = subtotal * percent;
        return total;
    }

    public double calculateSubtotal(double minutesCost, double planValue){
        double subtotal = planValue + minutesCost;
        return subtotal;
    }

    public double calculateTotalBill(double subtotal, double taxValue){
        double total = subtotal + taxValue;
        return total;
    }
}
