package classAndObjects.phoneBill;

public class PhoneBill {

    private int id;
    private double planValue;
    private int minutesUsed;
    private int allottedMinutes;

    public PhoneBill(int id, double planValue, int minutesUsed, int allottedMinutes){
        this.id = id;
        this.planValue = planValue;
        this.minutesUsed = minutesUsed;
        this.allottedMinutes = allottedMinutes;
    }

    public PhoneBill(int id){
        this(1,35,30,50);
    }

    public PhoneBill(int id, int minutesUsed){
        this(id,35,minutesUsed,50);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPlanValue() {
        return planValue;
    }

    public void setPlanValue(double planValue) {
        this.planValue = planValue;
    }

    public int getMinutesUsed() {
        return minutesUsed;
    }

    public void setMinutesUsed(int minutesUsed) {
        this.minutesUsed = minutesUsed;
    }

    public int getAllottedMinutes() {
        return allottedMinutes;
    }

    public void setAllottedMinutes(int allottedMinutes) {
        this.allottedMinutes = allottedMinutes;
    }
}
