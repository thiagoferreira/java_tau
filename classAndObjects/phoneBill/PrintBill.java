package classAndObjects.phoneBill;

public class PrintBill {

    public void printPhoneBill(PhoneBill phoneBill){
        Calculator calc = new Calculator();
        double planValue = phoneBill.getPlanValue();
        double minutesExCost = calc.calculateExMinutesCost(phoneBill.getMinutesUsed(), phoneBill.getAllottedMinutes());
        double subtotal = calc.calculateSubtotal(minutesExCost,planValue);
        double taxValue = calc.calculateTaxValue(subtotal);
        double total = calc.calculateTotalBill(subtotal,taxValue);
        System.out.println("Phone Bill Statement");
        System.out.println("Id: "+phoneBill.getId());
        System.out.println("Plan: $"+planValue);
        System.out.println("Overage Exceed minutes cost: $"+ minutesExCost);
        System.out.println("Tax: $"+taxValue);
        System.out.println("Total: $"+total);
    }
}
