package classAndObjects;

public class HomeAreaCalculator {

    public static void main(String args []){
        Rectangle room1 = new Rectangle();
        room1.setWidth(100);
        room1.setLength(200);

        double areaRoom1 = room1.calculateArea();

        Rectangle room2 = new Rectangle(25,50);
        double areaRoom2 = room2.calculateArea();

        double totalArea = areaRoom1 + areaRoom2;

        System.out.println("Are of both rooms: "+ totalArea);
    }
}
