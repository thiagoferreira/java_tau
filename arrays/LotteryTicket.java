package arrays;

import java.util.Arrays;
import java.util.Random;

public class LotteryTicket {

    private static final int LENGTH = 5;
    private static final int MAX_TICKET_NUMBER = 50;
    private static final int LENGTH_STAR = 2;
    private static final int MAX_TICKET_NUMBER_STAR = 12;

    public static void main(String[] args) {
        int[] ticket = generateNumbers();
        int[] stars = generateNumbersStar();
        Arrays.sort(ticket);
        printTicket(ticket);
        printStar(stars);
    }

    public static int[] generateNumbers(){
        int[] ticket = new int[LENGTH];
        Random random = new Random();
        for (int i=0; i<LENGTH; i++){
            int randomNumber;
            do {
                randomNumber = random.nextInt(MAX_TICKET_NUMBER)+1;
            }while (search(ticket,randomNumber));
            ticket[i] = randomNumber;
        }
        return ticket;
    }

    public static int[] generateNumbersStar(){
        int[] ticket = new int[LENGTH_STAR];
        Random random = new Random();
        for (int i=0; i<LENGTH_STAR; i++){
            int randomNumber;
            do {
                randomNumber = random.nextInt(MAX_TICKET_NUMBER_STAR)+1;
            }while (search(ticket,randomNumber));
            ticket[i] = randomNumber;
        }
        return ticket;
    }

    public static void printTicket(int ticket[]){
        for (int i=0; i<LENGTH; i++){
            System.out.print(ticket[i] + "|");
        }
    }

    public static void printStar(int ticket[]){
        for (int i=0; i<LENGTH_STAR; i++){
            System.out.print("\n"+ticket[i] + "|");
        }
    }

    /**
     * Does a sequential search on the array to find a value
     * @param array Array to search trough
     * @param numberToSearchFor Value to search for
     * @return tru if found, false if not
     */
    public static boolean search(int[] array, int numberToSearchFor){
         /*This is called an enhanced loop.
          It iterates through 'array' and
          each time assigns the current element to 'value'
         */
        for (int value : array){
            if (value == numberToSearchFor){
                return true;
            }
        }
        return false;
    }

    public static boolean binarySearch(int[] array, int numberToSearch){
        Arrays.sort(array);
        int index = Arrays.binarySearch(array,numberToSearch);
        if(index >=0){
            return true;
        }
        else return false;
    }
}
