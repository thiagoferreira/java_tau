package arrays;

import java.lang.reflect.Array;
import java.util.Scanner;

public class DayOfWeek {

    public static void main(String[] args) {
        String[] week = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number for the day of the week");
        int number = scanner.nextInt();
        scanner.close();
        System.out.println("Corresponding day: " + week[number - 1]);
    }


}
