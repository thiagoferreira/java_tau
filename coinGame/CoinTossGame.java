package coinGame;

import java.util.Scanner;

public class CoinTossGame {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Coin coin = new Coin();

        Player player1 = new Player(askPlayerName());
        Player player2 = new Player(askPlayerName());
        System.out.println("Player 1 "+player1.getName()+" choose Heads or Tails:");
        String chosenSide = scanner.next();
        boolean isGuessValid = isGuessValid(chosenSide);
        if(isGuessValid == true){
            player1.setGuess(chosenSide);
            coin.setSide(coin.flip());
            determineWinner(coin,player1,player2);
        }else System.out.println("Chosen value must be Heads or Tails");

        scanner.close();
    }

    public static String askPlayerName(){
        System.out.println("Enter the player's name:");
        return scanner.next();
    }

    public static boolean isGuessValid(String guess){
        if(guess.equalsIgnoreCase(Coin.HEADS) || guess.equalsIgnoreCase(Coin.TAILS)){
            return true;
        }
        return false;
    }

    public static void determineWinner(Coin coin, Player player1, Player player2){
        if(player1.getGuess().equals(coin.getSide())){
            System.out.println(player1.getName()+" is the Winner");
        }else System.out.println(player2.getName()+" is the Winner");
    }

}
