package coinGame;

import java.util.Random;

public class Coin {

    private String side;

    public static final String HEADS = "Heads";
    public static final String TAILS = "Tails";

    public String flip(){
        System.out.println("Flipping the coin...");
        String [] arr = {HEADS, TAILS};
        Random random = new Random();
        int select = random.nextInt(arr.length);
        System.out.println(arr[select]);
        return arr[select];
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }
}
