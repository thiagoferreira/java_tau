package exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
 /*       createNewFile();
        numbersExceptionHandling();*/
        divideByZero();
    }

    public static void createNewFile(){
        File file = new File("test/anyfile.txt");
        try {
            file.createNewFile();
        }catch (IOException e){
            System.out.println("Directory doesn't exist");
            e.printStackTrace();
        }
    }

    public static void numbersExceptionHandling(){
        File file = new File("test/anyfile.txt");
        Scanner fileReader = null;
        try {
            fileReader = new Scanner(file);
            while (fileReader.hasNext()) {
                double num = fileReader.nextDouble();
                System.out.println(num);
            }
        }catch (FileNotFoundException | InputMismatchException e){
            e.printStackTrace();
        }
        finally {
            fileReader.close();
        }
    }

    public static void createNewFileRethrow() throws IOException{
        File file = new File("test/anyfile.txt");
        file.createNewFile();
    }

    public static void divideByZero(){
        try {
            int c = 30/0;
            System.out.println(c);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println("Division is fun");
        }

    }
}
