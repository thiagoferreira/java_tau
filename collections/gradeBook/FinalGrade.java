package collections.gradeBook;

import java.util.HashMap;
import java.util.Map;

public class FinalGrade extends TestResults{

    public static void main(String[] args) {
        Map firstTest = getOriginalGrades();
        Map secondTest = getMakeUpGrades();
        Map finalGrade = getFinalGrade(firstTest,secondTest);
        printFinalGrade(finalGrade);
    }

    public static Map getFinalGrade(Map<String, Integer> firstTest, Map<String, Integer> secondTest){
        Map finalGrade = new HashMap();
        for(var student : firstTest.entrySet()){
            int first = firstTest.get(student.getKey());
            int second = secondTest.get(student.getKey());
            if(second>first){
                finalGrade.put(student.getKey(),second);
            }
            else
            {
                finalGrade.put(student.getKey(), student.getValue());
            }
        }
        return finalGrade;
    }

    public static void printFinalGrade(Map finalGrade){
        finalGrade.forEach(
                (k,v)->System.out.println("Student: " + k + ", Score: " + v));
    }
}
