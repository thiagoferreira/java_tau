package abstraction.farm;

public class Farm {
    public static void main(String[] args) {
        Animal duck = new Duck();
        duck.eat();
        duck.makeSound();
        Animal pig = new Pig();
        pig.eat();
        pig.makeSound();
    }
}
