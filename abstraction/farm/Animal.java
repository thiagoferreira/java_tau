package abstraction.farm;

public abstract class Animal {
    public abstract void makeSound();

    public void eat(){
        System.out.println("This animal is eating");
    }
}
