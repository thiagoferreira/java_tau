package inheritance.cake;

public class TasteTester {
    public static void main(String[] args) {
        BirthdayCake birthdayCake = new BirthdayCake("chocolate");
        WeddingCake weddingCake = new WeddingCake("pineapple");
        birthdayCake.setPrice(10);
        weddingCake.setPrice(100);

        System.out.println("Birthday Cake, price: "+birthdayCake.getPrice()+" flavor: "+birthdayCake.getFlavor());
        System.out.println("Wedding Cake, price: "+weddingCake.getPrice()+" flavor: "+weddingCake.getFlavor());
    }
}
