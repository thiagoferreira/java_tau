package javaFundamentals;

import java.util.Scanner;

public class MadLibs {
    public static void main(String args []) {
        System.out.println("Type a season of the year");
        Scanner scanner = new Scanner(System.in);
        String season = scanner.next();

        System.out.println("Type a whole number");
        int number = scanner.nextInt();

        System.out.println("Type an adjective");
        String adjective = scanner.next();

        System.out.println("On a "+adjective+" "+season+" day, I drink a minimum of "+number+" cups of coffee");

    }
}
