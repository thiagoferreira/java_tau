package javaFundamentals;

import java.util.Scanner;

public class GrossPayCalculator {
    public static void main(String[] args) {

        System.out.println("Enter the number of hours the employee worked");
        Scanner scanner = new Scanner(System.in);
        double hoursWorked = scanner.nextDouble();

        System.out.println("Enter the employee's pay rate");
        double payRate = scanner.nextDouble();

        double grossPay = hoursWorked * payRate;

        System.out.println("The employee's gross salary is:"+ grossPay);
    }
}