package loops;

import java.util.Random;
import java.util.Scanner;

public class RollDiceGame {

    public static void main(String args []){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 1 to start the game");
        int input = scanner.nextInt();
        while(input != 1){
            System.out.println("Enter 1 to start the game");
            input = scanner.nextInt();
        }

        int space = 0;
        int spaceToGo = 20;
        for(int i=0; i<5; i++ ){
            Random random = new Random();
            int dice = random.nextInt(6) + 1;
            space = space + dice;
            System.out.println("Roll #"+(i+1)+": You've rolled a "+dice+". You are now on space "+space+" and have "
                    +(spaceToGo-space)+" more to go.");
            if(space == spaceToGo){
                System.out.println("Congrats, You Won :D");
                break;
            } else if (space > spaceToGo) {
                System.out.println("You lose :( You got more than 20");
            } else if (i == 4 && space < spaceToGo) {
                System.out.println("You lose :( You got less than 20");
            }
        }

    }
}